const express = require("express");
const axios = require("axios");
const cheerio = require("cheerio");
const ytdl = require("ytdl-core");

const app = express();
const PORT = 3000;

app.get("/download", async (req, res) => {
  const videoUrl = req.query.url;
  const platform = req.query.platform;

  if (!videoUrl || !platform) {
    return res.status(400).json({ error: "video link and platform are required" });
  }

  switch (platform) {
    case "instagram":
      try {
        const response = await axios.get(videoUrl);
        const html = response.data;
        const $ = cheerio.load(html);
        const videoSrc = $('meta[property="og:video"]').attr("content");

        if (!videoSrc) {
          return res.status(400).json({ error: "video URL is not present in this post" });
        }

        const videoResponse = await axios.get(videoSrc, {
          responseType: "stream",
        });

        videoResponse.data.pipe(res);
      } catch (error) {
        console.log("Error:", error);
        res.status(500).json({ error: "There was an error when downloading the video" });
      }
      break;

    case "youtube":
      try {
        const videoResponse = await axios.get(videoUrl, {
          responseType: "stream",
        });

        videoResponse.data.pipe(res);
      } catch (error) {
        console.log("Error:", error);
        res.status(500).json({ error: "There was an error when downloading the video" });
      }
      break;

    default:
      return res.status(400).json("invalid platform selected");
  }
});

app.get("/getinfo", async (req, res) => {
  const videoUrl = req.query.url;

  if (!videoUrl) {
    return res.status(400).json({ error: "you have to provide a video URL" });
  }

  try {
    const info = await ytdl.getInfo(videoUrl);
    return res.json(info);
  } catch (error) {
    console.log("Error:", error);
    return res.status(400).json({ error: "Info cannot be obtained" });
  }
});

app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});